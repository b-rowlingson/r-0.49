TITLE(chisquare.test @@ Chi-Square Test)
USAGE(
chisquare.test(x, p=rep(1/length(x),length(x)))
)
DESCRIPTION(
LANG(chisquare.test) performs a chisquare test on the
contingency table in the object LANG(x).
If LANG(x) is a vector or a matrix with one row or column,
the table is assumed to be one-dimensional, and the chisquare
test for a specific set of population probabilities is performed.
These are in the vector LANG(p), or, if LANG(p) is missing,
are assumed to be equal.
If LANG(x) is a matrix with two or more rows and
two or more columns, a chisquare test for independence or equal
group probabilities is performed on the contingency table in LANG(x).
)
VALUE(
The function prints out the result of the test and returns a
list containing the following elements:
ARG(chi @@ the matrix containing the components of chi-square.)
ARG(E @@ the matrix containing the expected values.)
)
