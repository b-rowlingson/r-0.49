TITLE(predict @@  Regression Prediction)
USAGE(
predict(reglist, DOTS)
)
ARGUMENTS(
ARG(reglist @@ a list produced by LANG(regress).)
ARG(DOTS @@ either a series of vectors, all of the same length,
separated by commas, or a matrix.
If the former, the vectors contain the
data for the explanatory variables for which we wish to make predictions.
If a matrix, then the columns contain the data for the explanatory variables.)
)
DESCRIPTION(
LANG(predict) calculates prediction intervals for regression.
Returns a matrix, the rows of which correspond to separate predictions at
different x-values (the number of rows equals the length of the vectors
in LANG(DOTS), or the number of rows if LANG(DOTS) is a matrix),
and the columns of which contain the value
of the prediction, the upper and lower limits for a confidence
interval for the mean of all responses taken at x,
and the upper and lower limits of a prediction
interval for an observation taken at x.
)
