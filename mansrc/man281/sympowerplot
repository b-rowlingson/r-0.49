TITLE(sympowerplot @@ Plot Symmetry Statistic versus Power)
USAGE(
sympowerplot(x, p=seq(-1, 2, length=20), alpha=0.05)
)
ARGUMENTS(
ARG(x @@ a numeric vector containing the data.)
ARG(p @@ a vector containing the powers to transform the data.)
ARG(alpha @@ the quantile to use in computing the symmetry statistic.)
)
DESCRIPTION(
The data LANG(x) are transformed via a power transformation according to 
the powers given in LANG(p). For each transformed data set the symmetry
statistic, EQN((med - SUB(q,greekalpha) )/(SUB(q,{1 - greekalpha})
- SUB(q,greekalpha)))
is computed.
The symmetry statistic is plotted against the power and
a horizontal line is plotted at 0.5.
This quantity is approximately 0.5 when the distribution is symmetric.
To find an approximate symmeterizing transformation use this function
and pick as your power the value of LANG(p) where the symmetry statistic
crosses 0.5.
)
