TITLE(Beta @@ The Beta Distribution)
USAGE(
dbeta(x, a, b)
pbeta(q, a, b)
qbeta(p, a, b)
rbeta(n, a, b)
)
ALIAS(dbeta)
ALIAS(pbeta)
ALIAS(qbeta)
ALIAS(rbeta)
VALUE(
These functions provide information about the Beta distribution
with parameters LANG(a) and LANG(b).  LANG(dbeta) gives the density,
LANG(pbeta) the distribution function, LANG(qbeta) the
quantile function and LANG(rbeta) generates random deviates.
PARA
The Beta distribution has density
DEQN(f(x) =
OVER(greekGamma (a + b ) @@ greekGamma (a) greekGamma (b))
SUP(x@@a) SUP((1-x)@@b) @@ Gamma(a+b)/(Gamma(a)Gamma(b))x^(a-1)(1-x)^(b-1))
for EQN(a GT 0),
EQN(b GT 0)
and
EQN(0 LT x LT 1).
)
