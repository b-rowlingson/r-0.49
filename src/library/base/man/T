TITLE(T @@ The Student t Distribution)
USAGE(
dt(x, df)
pt(q, df)
qt(p, df)
rt(n, df)
)
ALIAS(dt)
ALIAS(pt)
ALIAS(qt)
ALIAS(rt)
DESCRIPTION(
These functions provide information about the t distribution with
LANG(df) degrees of freedom.  LANG(dt) gives the density, LANG(pt)
gives the distribution function LANG(qt) gives the quantile function
and LANG(rt) generates random deviates.
PARA
The t distribution with EQN(n) degrees of freedom has density
DEQN(
f(x) = OVER(greekGamma ((n+1)/2)@@
SQRT(greekpi n) greekGamma (n/2))
(1 + x^2/n)^{-(n+1)/2}
@@
f(x) = Gamma((n+1)/2) / (sqrt(n pi) Gamma(n/2))
            (1 + x^2/n)^-((n+1)/2))
for all real EQN(x).
)
EXAMPLES(
1 - pt(1:5, df = 1)
qt(.975, df = c(1:10,20,50,100,1000))
)
