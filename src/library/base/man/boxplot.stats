TITLE(boxplot.stats @@ Box Plot Statistics)
USAGE(
boxplot.stats(x, coef)
)
ALIAS(boxplot.stats)
ARGUMENTS(
ARG(x @@ a numeric vector for which the boxplot will
be constructed (LANG(NA)s are allowed).)
ARG(coef @@ this determines how far the plot ``whiskers''
extend out from the box.
If LANG(coef) is positive, 
the whiskers extend to the most extreme data point which
is no more than LANG(coef) times the interquartile coef
from the box.
A value of zero causes the whiskers to extend to the data extremes.)
)
VALUES(
This function is typically is called by LANG(boxplot) to
gather the statistics necessary for producing box plots,
but may be invoked separately.  The value it returns is
a list with named components as follows: @@
ARG(stats @@ a vector containing the extreme of the
lower whisker, the lower ``hinge'', the median, the upper ``hinge''
and the extreme of the upper whisker.)
ARG(n @@ the number of observations in the sample.)
ARG(conf @@ the lower and upper extremes of the ``notch''.)
ARG(out @@ the values of any data points which lie beyond the
extremes of the whiskers.)
)
SEEALSO(
LANG(LINK(boxplot)), LANG(LINK(bxp)).
)
EXAMPLES(
boxplot.stats(1:100)
)
