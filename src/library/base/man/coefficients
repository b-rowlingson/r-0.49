TITLE(coefficients @@ Extract Model Coefficents)
USAGE(
coefficients(x, DOTS)
coef(x, DOTS)
)
ALIAS(coefficients)
ALIAS(coef)
ARGUMENTS(
ARG(x @@ an object for which the extraction of model
coefficients is meaningful.)
ARG(DOTS @@ other arguments.)
)
VALUE(
These are generic functions which extract
model coefficents from objects returned by modeling functions.
The abbreviated form LANG(coef) is intended to encourage users
to access object components through an accessor function
rather than by directly referencing an object slot.
PARA
All object classes which are returned by model fitting
functions should provide a LANG(coefficients) method.
)
SEEALSO(
LANG(LINK(fitted.values)), LANG(LINK(glm)), LANG(LINK(lm)),
LANG(LINK(residuals)).
)
