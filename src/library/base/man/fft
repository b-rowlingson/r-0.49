TITLE(fft @@  Discrete Fourier Transform)
USAGE(
fft(z, inverse=F)
mvfft(z, inverse=F)
)
ALIAS(fft)
ALIAS(mvfft)
ARGUMENTS(
ARG(z @@ a real or complex value array containing the
values to be transformed).
ARG(inverse @@ if LANG(TRUE), the inverse transform is computed
(the inverse has a LANG(+) in the exponent of ITALIC(e) and
divides the values by LANG(1/length(x)) before transforming).))
VALUE(
When LANG(z) is a vector, the value computed and returned by LANG(fft) is
the univariate Fourier transform of the sequence of values in LANG(z).
When LANG(z) contains an array, LANG(fft) computes and returns
the multivariate (spatial) transform.
PARA
By contrast, LANG(mvfft) takes a real or complex matrix as argument,
and returns a similar shaped matrix, but with each column
replaced by its discrete Fourier transform.
This is useful for analysing vector-valued series.
PARA
The FFT is fastest when the length of of the series being
transformed is highly composite (i.e. has many factors).
If this is not the case, the transform may take a long time
to compute and will use a large amount of memory.
)
REFERENCES(
Singleton, R. C. (1979).
Mixed Radix Fast Fourier Transforms,
in ITALIC(Programs for Digital Signal Processing),
IEEE Digital Signal Processing Committee eds.
IEEE Press.
)
SEEALSO(
LANG(LINK(convolve)), LANG(LINK(nextn)).
)
EXAMPLES(
plot(fft(c(9:0,0:13, numeric(301))),type='l')
periodogram <- function(x) { ##-- simple periodogram of x[]
	n <- length(x)
	Mod(fft(unclass(x)))[1:(n%/%2 + 1)]^2 / (2*pi*n)
}
data(sunspots)
plot(10*log10(periodogram(sunspots)),type='b', col='blue')
)
