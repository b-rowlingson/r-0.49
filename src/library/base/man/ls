TITLE(ls @@ List Objects)
USAGE(
ls(name, pos=2, envir=sys.frame(sys.parent()),
       all.names=FALSE, pattern)
objects(name, pos=2, envir=sys.frame(sys.parent()),
       all.names=FALSE, pattern)
)
ALIAS(ls)
ALIAS(objects)
ARGUMENTS(
ARG(name @@ the name of an attached object appearing
in the vector of names returned by LANG(search).)
ARG(pos @@ the index of an attached object in
the list returned by LANG(search).)
ARG(envir @@ an evaluation environment.)
ARG(all.names @@ a logical value.  If LANG(TRUE), all
object names are returned.  If LANG(FALSE) names
which begin with a ``.'' are omitted.)
ARG(pattern @@ an optional regular expression.
Only names matching LANG(pattern) are returned.)
)
DESCRIPTION(
LANG(ls) and LANG(objects) return a vector of character strings
giving the names of the objects in the specified environment.
When invoked with no argument at the top level prompt,
LANG(ls) shows what data sets and functions a user has defined.
When invoked with no argument inside a function,
LANG(ls) returns the names of the functions local variables.
This is useful in conjunction with
LANG(browser).
)
