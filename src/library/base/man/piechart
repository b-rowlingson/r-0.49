TITLE(piechart @@ Pie Charts)
USAGE(
piechart(x, labels=names(x), shadow=FALSE,
        edges=200, radius=0.8, fill=NULL, main=NULL, DOTS)
)
ALIAS(piechart)
ARGUMENTS(
ARG(x @@ a vector of positive quantities.
The values in LANG(x) are displayed as the areas of pie slices.)
ARG(labels @@ a vector of character strings giving names for
the slices.)
ARG(shadow @@ a logical vector indicating whether a shadow
effect should be attempted for the chart.
This only makes sense if the slices are filled with colors.)
ARG(edges @@ the circular outline of the pie is approximated by a
polygon with this many edges.)
ARG(radius @@ the pie is drawn centered in a square box
whose sides range from EQN(-1) to EQN(1).
If the character strings labeling the slices are long it
may be necessary to use a smaller radius.)
ARG(col @@ a vector of colors to be used in filling
the slices.)
ARG(main @@ an overall title for the plot.)
ARG(DOTS @@ graphical parameters can be given as arguments to LANG(piechart).)
)
DESCRIPTION(
Pie charts are a very bad way of displaying information.
The eye is good at judging linear measures and bad at judging relative areas.
A bar chart or dot chart is a preferable way of displaying this
type of data.
)
SEEALSO(
LANG(LINK(dotplot)).
)
EXAMPLES(
piechart(rep(1,24), col=rainbow(24), radius=0.9)
PARA
pie.sales <- c(0.12, 0.3, 0.26, 0.16, 0.04, 0.12)
names(pie.sales) <- c("Blueberry", "Cherry",
    "Apple", "Boston Cream", "Other", "Vanilla Cream")
piechart(pie.sales,
    col=c("purple", "violetred1", "green3",
    "cornsilk", "cyan", "white"))
piechart(pie.sales,
    col=gray(seq(0.4,1.0,length=6)))
)
