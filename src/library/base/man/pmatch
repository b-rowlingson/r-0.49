TITLE(pmatch @@ Partial String Matching)
USAGE(
   pmatch(x, table, nomatch = NA)
charmatch(x, table, nomatch = NA)
)
ALIAS(pmatch)
ALIAS(charmatch)
ARGUMENTS(
ARG(x @@ the values to be matched.)
ARG(table @@ the values to be matched against.)
ARG(nomatch @@ the value returned at non-matching positions.)
)
DESCRIPTION(
LANG(pmatch) seeks matches for the elements of its first argument
among those of its second.  If there is a unique match then the index
of the matching value is returned; if multiple matches are found then
LANG(0) is returned and if no match is found then LANG(NA) is
returned.
PARA
This function is based on Terry Therneau's LANG(charmatch) S function.
In R, LANG(charmatch) is synonym to LANG(pmatch).
)
SEEALSO(
LANG(LINK(match)).
)
EXAMPLES(
pmatch("m",   c("mean", "median", "mode")) # returns 0
pmatch("med", c("mean", "median", "mode")) # returns 2
)

