/*
 *  R : A Computer Langage for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "Defn.h"

	/*--- I / O -- S u p p o r t -- C o d e ---*/

	/* These routines provide hooks for supporting console */
	/* I/O.  Under raw Unix these routines simply provide a */
	/* connection to the stdio library.  Under a Motif */
	/* interface the routines would be considerably more */
	/* complex. */


	/* Fill a text buffer with user typed console input. */
	/* This routine is only called when R_Console == 1. */

#ifdef HAVE_LIBREADLINE

char *readline(char *);
char R_prompt_buf[512];

int ReadKBD(char *buf, int len)
{
	int l;
	char *rline;
	if(!isatty(0)) {
		if(!R_Quiet && *R_prompt_buf) fputs(R_prompt_buf, stdout);
		if (fgets(buf, len, stdin) == NULL) 
			return 0;
		else {
			if(!R_Quiet) fputs(buf,stdout);
			return 1;
		}
	}
	else {
		if( *R_prompt_buf) {
			rline = readline(R_prompt_buf);
			if (rline) {
				if (strlen(rline) && *R_prompt_buf)
					add_history(rline);
				l = (((len-2) > strlen(rline)) ? strlen(rline) : (len-2));
				strncpy(buf, rline, l);
				buf[l] = '\n'; buf[l+1]= '\0';
				free(rline);
				return 1;
			}
			else return 0;
		}
		else {
			if(fgets(buf, len, stdin) == NULL)
				return 0;
			else
				return 1;
		}
	}
}

#else /* not HAVE_LIBREADLINE */

int ReadKBD(char *buf, int len)
{
	if (fgets(buf, len, stdin) == NULL)
		return 0;
	else {
		if(!R_Quiet && !isatty(0) )
			fputs(buf,stdout);
		return 1;
	}
}
#endif

	/* Write a text buffer to the console. */
	/* All system output is filtered through this routine. */

int RWriteConsole(char *buf, int len)
{
	printf("%s", buf);
}


	/* Reset so that input comes from the console This is used */
	/* after error messages so that if the system was reading */
	/* from a file, input is redirected from the console */

void ResetConsole()
{
	R_Console = 1;
	R_Inputfile = stdin;
}


	/* This is stdio support to ensure that console file buffers */
	/* are flushed. */

void FlushConsole()
{
	if (R_Console == 1)
		fflush(stdin);
}


	/* This is stdio support to reset if the used types EOF */
	/* on the console. */

void ClearerrConsole()
{
	if (R_Console == 1)
		clearerr(stdin);
}


void Consolegets(char *buf, int buflen)
{
	fgets(buf, buflen, stdin);
	if( !isatty(0) )
		fputs(buf,stdout);
}


	/*--- F i l e    H an d l i n g    C o d e ---*/

FILE *R_OpenLibraryFile(char *file)
{
	char buf[256], *home;
	FILE *fp;

	if((home = getenv("RHOME")) == NULL)
		return NULL;
	sprintf(buf, "%s/library/%s", home, file);
	fp = fopen(buf,"r");
	return fp;
}

FILE *R_OpenSysInitFile(void)
{
	char buf[256];
	FILE *fp;

	sprintf(buf, "%s/library/Rprofile", getenv("RHOME"));
	fp = fopen(buf, "r");
	return fp;
}

FILE *R_OpenInitFile(void)
{
	char buf[256];
	FILE *fp;

	fp = NULL;

	if(fp = fopen(".Rprofile", "r"))
		return fp;

	sprintf(buf, "%s/.Rprofile", getenv("HOME"));
	if(fp = fopen(buf, "r"))
		return fp;

	return fp;
}


	/*--- I n i t i a l i z a t i o n    C o d e ---*/

#ifdef HAVE_TIMES
#include <time.h>
#include <sys/times.h>

static int StartTime;
#endif

#ifdef __FreeBSD__
#include <floatingpoint.h>
#endif

#ifdef linux
#include <fpu_control.h>
#endif

int main(int ac, char **av)
{
	int value;
	char *p;

#ifdef HAVE_TIMES
	StartTime = time(NULL);
#endif
	R_Quiet = 0;

	while(--ac) {
		if(**++av == '-') {
			switch((*av)[1]) {
			case 'v':
				if((*av)[2] == '\0') {
					ac--; av++; p = *av;
				}
				else p = &(*av)[2];
				value = strtol(p, &p, 10);
				if(*p) goto badargs;
				if(value < 1 || value > 1000)
					REprintf("warning: invalid vector heap size ignored\n");
				else
					R_VSize = value * 1048576;  /* Mb */
				break;
			case 'n':
				if((*av)[2] == '\0') {
					ac--; av++; p = *av;
				}
				else p = &(*av)[2];
				value = strtol(p, &p, 10);
				if(*p) goto badargs;
				if(value < R_NSize || value > 1000000)
					REprintf("warning: invalid language heap size ignored\n");
				else
					R_NSize = value;
				break;
			case 'q':
				R_Quiet = 1;
				break;
			default:
				REprintf("warning: unknown option %s\n", *av);
				break;
			}
		}
		else {
			printf("ARGUMENT %s\n", *av);
		}

	}
	
		/* On Unix the console is a file */
		/* we just use stdio to write on it */

	R_Interactive = isatty(0);
	R_Consolefile = stdout;
	R_Outputfile = stdout;
	R_Sinkfile = NULL;

#ifdef __FreeBSD__
	fpsetmask(0);
#endif

#ifdef linux
	/* FIXME: find the fenv.h equivalent */
	/* __setfpucw(_FPU_IEEE); */
#endif

#ifdef HAVE_LIBREADLINE
	if(isatty(0))
		read_history(".Rhistory");
#endif
	mainloop();
	return 0;

badargs:
	REprintf("invalid argument passed to R\n");
	exit(1);
}

void R_InitialData(void)
{
	R_RestoreGlobalEnv();
}

void RCleanUp(int ask)
{
	char buf[128];

	if( R_DirtyImage ) {
qask:
		ClearerrConsole();
		FlushConsole();
		if(!isatty(0) && ask==1)
			ask = 3;

		if( ask==1 ) {
			REprintf("Save workspace image? [y/n/c]: ");
			Consolegets(buf, 128);
		}
		else if(ask == 2)
			buf[0] = 'n';
		else if (ask == 3)
			buf[0] = 'y';

		switch (buf[0]) {
		case 'y':
		case 'Y':
			R_SaveGlobalEnv();
#ifdef HAVE_LIBREADLINE
			if(isatty(0))
				write_history(".Rhistory");
#endif
			break;
		case 'n':
		case 'N':
			break;
		case 'c':
		case 'C':
			jump_to_toplevel();
			break;
		default:
			goto qask;
		}
	}
	KillDevice();

#ifdef __FreeBSD__
	fpsetmask(~0);
#endif

#ifdef linux
	/* FIXME: find the fenv.h equivalent */
	/*	__setfpucw(_FPU_DEFAULT); */
#endif

	exit(0);
}

void RBusy(int which)
{
}

        /* Saving and Restoring the Global Environment */

void R_SaveGlobalEnv(void)
{
	FILE *fp = fopen(".RData", "w");
	if (!fp)
		error("can't save data -- unable to open ./.Rdata\n");
	R_WriteMagic(fp, R_MAGIC_BINARY);
	BinarySave(FRAME(R_GlobalEnv), fp); 
	fclose(fp);
}

SEXP BinaryLoadOld(FILE*, int);
SEXP AsciiLoadOld(FILE*, int);

void R_RestoreGlobalEnv(void)
{
	FILE *fp;
	if(!(fp = fopen(".RData","r"))) {
		/* warning here perhaps */
		return;
	}
	if(!R_Quiet)
		Rprintf("[Previously saved workspace restored]\n\n");

	switch(R_ReadMagic(fp)) {
	case R_MAGIC_BINARY:
		FRAME(R_GlobalEnv) = BinaryLoad(fp);
		break;
	case R_MAGIC_ASCII:
		FRAME(R_GlobalEnv) = AsciiLoad(fp);
		break;
	case R_MAGIC_BINARY_VERSION16:	/* Version 0.16 compatibility */
		FRAME(R_GlobalEnv) = BinaryLoadOld(fp, 16);
		break;
	case R_MAGIC_ASCII_VERSION16:	/* Version 0.16 compatibility */
		FRAME(R_GlobalEnv) = AsciiLoadOld(fp, 16);
		break;
	default:
		fclose(fp);
		error("workspace file corrupted -- no data loaded\n");
	}
}

	/*--- P l a t f o r m -- D e p e n d e n t -- F u n c t i o n s ---*/

#ifdef HAVE_TIMES
#ifndef CLK_TCK
/* this is in ticks/second, generally 60 on BSD style Unix, 100? on SysV */
#ifdef HZ
#define CLK_TCK HZ
#else
#define CLK_TCK	60
#endif
  
#endif

SEXP do_proctime(SEXP call, SEXP op, SEXP args, SEXP env)
{
	SEXP ans;
	struct tms timeinfo;
	clock_t elapsed;
	elapsed = time((clock_t)NULL) - StartTime;
	(void)times(&timeinfo) ;
	ans = allocVector(REALSXP, 5);
	REAL(ans)[0] = timeinfo.tms_utime / (double)CLK_TCK;
	REAL(ans)[1] = timeinfo.tms_stime / (double)CLK_TCK;
	REAL(ans)[2] = elapsed;
	REAL(ans)[3] = timeinfo.tms_cutime / (double)CLK_TCK;
	REAL(ans)[4] = timeinfo.tms_cstime / (double)CLK_TCK;
	return ans;
}
#endif

SEXP do_machine(SEXP call, SEXP op, SEXP args, SEXP env)
{
	return mkString("Unix");
}

SEXP do_system(SEXP call, SEXP op, SEXP args, SEXP rho)
{
	FILE *fp;
	char *x = "r", buf[120];
	int read, i, j;
	SEXP tlist = R_NilValue, tchar, rval;

	checkArity(op, args);
	if (!isString(CAR(args)))
		errorcall(call, "character argument expected\n");
	if (isLogical(CADR(args)))
		read = INTEGER(CADR(args))[0];
	if (read) {
		PROTECT(tlist);
		fp = popen(CHAR(STRING(CAR(args))[0]), x);
		for (i = 0; fgets(buf, 120, fp); i++) {
			read = strlen(buf);
			buf[read - 1] = '\0';
			tchar = mkChar(buf);
			UNPROTECT(1);
			PROTECT(tlist = CONS(tchar, tlist));
		}
		pclose(fp);
		rval = allocVector(STRSXP, i);;
		for (j = (i - 1); j >= 0; j--) {
			STRING(rval)[j] = CAR(tlist);
			tlist = CDR(tlist);
		}
		UNPROTECT(1);
		return (rval);
	}
	else {
		tlist = allocVector(INTSXP, 1);
		INTEGER(tlist)[0] = system(CHAR(STRING(CAR(args))[0]));
		R_Visible = 0;
		return tlist;
	}
}

SEXP do_interactive(SEXP call, SEXP op, SEXP args, SEXP rho)
{
	SEXP rval;
 
	rval=allocVector(LGLSXP, 1); 
	if( isatty(0) )
		LOGICAL(rval)[0]=1;
	else
		LOGICAL(rval)[0]=0;
	return rval;
}

SEXP do_quit(SEXP call, SEXP op, SEXP args, SEXP rho)
{
        char *tmp;
        int ask;
 
        if(R_BrowseLevel) {
                warning("can't quit from browser\n"); 
                return R_NilValue;
        }
        if( !isString(CAR(args)) ) 
                errorcall(call,"one of \"yes\", \"no\" or \"ask\" expected.\n");
        tmp = CHAR(STRING(CAR(args))[0]);
        if( !strcmp(tmp,"ask") )
                ask=1;
        else if( !strcmp(tmp,"no") )
                ask=2;
        else if( !strcmp(tmp,"yes") )
                ask=3;
        else
                errorcall(call,"unrecognized value of ask\n");
        RCleanUp(ask);
        exit(0);
        /*NOTREACHED*/
}

	/* Declarations to keep f77 happy */

int MAIN_()  {return 0;}
int MAIN__() {return 0;}
int __main() {return 0;}
